// JavaScript Document
function showDiv() {
	var vp = 150;
    if ($(window).scrollTop() > vp && !($('#mainNav').data('positioned') == 'true')) {
        $("#mainNav").addClass("bg-nav").data('positioned', 'true');
    } else if ($(window).scrollTop() == 0) {
        $("#mainNav").removeClass("bg-nav").data('positioned', 'false');
    } else if ($(window).scrollTop() < vp) {
        $("#mainNav").removeClass("bg-nav").data('positioned', 'false');
    }
}
$(window).scroll(showDiv);

//

$(".navbar-toggle").click(function() {
	$("#mainNav").toggleClass("mobile-nav-bg");
});

$(".ul-map-tabs > li").on("click", function() {
	$(this).addClass("active").siblings(".active").removeClass("active");
	if ($(".tab-perth").hasClass("active")) {
		$(".map").addClass("perth-ofc");
		$(".map").removeClass("melbourne-ofc");
		$(".map").removeClass("queensland-ofc");
		$(".company-info .text-block.address").addClass("perth-ofc");
		$(".company-info .text-block.address").removeClass("melbourne-ofc");
		$(".company-info .text-block.address").removeClass("queensland-ofc");
	} else if ($(".tab-melbourne").hasClass("active")) {
		$(".map").addClass("melbourne-ofc");
		$(".map").removeClass("perth-ofc");
		$(".map").removeClass("queensland-ofc");
		$(".company-info .text-block.address").addClass("melbourne-ofc");
		$(".company-info .text-block.address").removeClass("perth-ofc");
		$(".company-info .text-block.address").removeClass("queensland-ofc");
	} else if ($(".tab-queensland").hasClass("active")) {
		$(".map").addClass("queensland-ofc");
		$(".map").removeClass("perth-ofc");
		$(".map").removeClass("melbourne-ofc");
		$(".company-info .text-block.address").addClass("queensland-ofc");
		$(".company-info .text-block.address").removeClass("perth-ofc");
		$(".company-info .text-block.address").removeClass("melbourne-ofc");
	}

});

// Smooth Anchors
$("#mainNav a[href^='#']").on('click', function(e) {
   e.preventDefault();
   $('html, body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
});